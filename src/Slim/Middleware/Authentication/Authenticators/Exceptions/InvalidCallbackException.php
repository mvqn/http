<?php
declare(strict_types=1);

namespace MVQN\HTTP\Slim\Middleware\Authentication\Authenticators\Exceptions;

final class InvalidCallbackException extends \Exception
{
}
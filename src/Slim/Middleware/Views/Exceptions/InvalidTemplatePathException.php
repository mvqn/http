<?php
declare(strict_types=1);

namespace MVQN\HTTP\Slim\Middleware\Views\Exceptions;

final class InvalidTemplatePathException extends \Exception
{
}
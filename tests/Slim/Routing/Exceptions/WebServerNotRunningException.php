<?php
declare(strict_types=1);

namespace MVQN\HTTP\Slim\Routing\Exceptions;

final class WebServerNotRunningException extends \Exception
{
}